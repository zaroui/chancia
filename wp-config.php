<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'chancia');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZgP9S|x6jddasNw8`bidwZ~Bt;N}Wn@v#&3cM%PAq&?PN|;%dk6A`2ASgZk-~lfg');
define('SECURE_AUTH_KEY',  '/*^Os`z$!QyP3P{e.acN:u$UL=b[=>fGRd]!eJ|X#t|vxQCsdhIQ3<YEPa5kb/8 ');
define('LOGGED_IN_KEY',    '!dvH*Xl<T./JU ``oT V^Fq1#+Akp2G*NPLj,t@+$=AOyK)2Wta_O7p)61#V5yRy');
define('NONCE_KEY',        '8Dz!YvKTZ)B&;9WjsKxsRsV~.C/h*&`7aM_Le7=wx~<`6>6JeI}e;_m~-X&qsRre');
define('AUTH_SALT',        'J,K``Aet1TP#WL2B6D7[xh>L9yr2hxIMq~s&r4GAe>,*1:I];&B_y}Xzz%9pIl5*');
define('SECURE_AUTH_SALT', '~fHw`MgetHuS]]FMJ.bz`ii@32`{H%H#K!+U-,`t84AY#i0l:jpRLVQS9a$:>^{7');
define('LOGGED_IN_SALT',   'o{G0gIg<N)MkWQN1E*+`Q(27aWw24-TLG. 4mgEO!fR `(PJ0S7]+[nxUZ,6[NP{');
define('NONCE_SALT',       'uXQZgV}/,^2ZMz,d]nhCtJ^RhFe-]SwZPnmp*I!KLZ]uD;Z&VfaxQ((G8pK21T_n');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
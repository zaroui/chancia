<?php get_header() ?>


<?php
    if(have_posts()) :

        while (have_posts()) : the_post() ;
            the_content();
        endwhile ;
    endif ;
    ?>
<?php

?>
<?php get_footer() ?>

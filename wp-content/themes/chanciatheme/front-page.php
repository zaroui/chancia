<?php /* Template Name: hotelModel */ ?>
<?php get_header() ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="search-container">
                <div class="search-section">
                    <h2>Réservez des hotels en tunisie à petit prix</h2>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-sm-12">
                            <form class="form search-form" action="chancia/search" method="GET">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group form-group-lg">
                                        <label class="control-label">Ou shouitez-vous séjourner</label>
                                        <input class="form-control" type="text" placeholder="Ex ville ou hotel spécifique"
                                        name="destination"/></div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group form-group form-group-lg "  >
                                        <label class="control-label">Arrivée &gt;&gt; Départ</label>
                                        <input class="form-control" type="text" placeholder="Mer, 10 Mai- Ven, 12 Mai"  id='datetimepicker'/></div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group form-group form-group-lg">
                                                <label class="control-label">Chambres</label>
                                                <input class="form-control" type="number" value="1" /></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-group form-group-lg"><label class="control-label">Adultes</label>
                                                <input class="form-control" type="number" value="1" name="adultes" /></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-group form-group-lg"><label class="control-label">Enfants</label>
                                                <input class="form-control" type="number" value="0" name="enfants" /></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group form-group form-group-lg">
                                    <input class="btn btn-lg search-btn" type="submit" value="Rechercher" /></div>
                                </div>
                            </form></div>
                    </div>
                </div>
            </div>
        </div>
        <!----- end header section -->
        <div class="col-md-12 best-of">
            <div>
                <h1 style="text-align: center;">Les meilleurs <span style="color: red;">o</span>ffres du moment</h1>
                <p style="text-align: center; margin-bottom: 5px;">Recherchez et réservez des séjours à bas prix et des hôtels bon marché avec</p>
                <p style="text-align: center; margin-bottom: 25px;">chancia Effectuez vos recherches sur des certaines d'hôtels en seule</p>
                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/line.png" alt="" width="170" height="2" />

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row visible-lg visible-md visible-sm hidden-xs">
        <div class="col-md-7 col-md-offset-3 col-sm-12">
            <ul id="offres-tabs" class="nav nav-tabs" role="tablist">
                <li class="active" role="presentation"><a role="tab" href="#tous" aria-controls="home" data-toggle="tab">Toutes les offres</a></li>
                <li role="presentation"><a role="tab" href="#hammamet" aria-controls="profile" data-toggle="tab">Hammamet</a></li>
                <li role="presentation"><a role="tab" href="#sousse" aria-controls="messages" data-toggle="tab">Sousse</a></li>
                <li role="presentation"><a role="tab" href="#monastir" aria-controls="settings" data-toggle="tab">Monastir</a></li>
                <li role="presentation"><a role="tab" href="#mahdi" aria-controls="settings" data-toggle="tab">Mahdia</a></li>
            </ul>
        </div>
    </div>
    <div class="row hidden-md hidden-lg hidden-sm visible-xs">
        <div class="col-xs-11 col-xs-offset-1"><nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <ul id="offres-tabs-sm" class="nav nav-tabs" role="tablist">
                        <li class="active" role="presentation"><a role="tab" href="#tous-sm" aria-controls="home" data-toggle="tab">Toutes les offres</a></li>
                        <li role="presentation"><a role="tab" href="#hammamet-sm" aria-controls="profile" data-toggle="tab">Hammamet</a></li>
                        <li role="presentation"><a role="tab" href="#sousse-sm" aria-controls="messages" data-toggle="tab">Sousse</a></li>
                        <li role="presentation"><a role="tab" href="#monastir-sm" aria-controls="settings" data-toggle="tab">Monastir</a></li>
                        <li role="presentation"><a role="tab" href="#mahdis-sm" aria-controls="settings" data-toggle="tab">Mahdia</a></li>
                    </ul>
                </div>
            </nav></div>
    </div>
    <div class="visible-lg visible-md visible-sm hidden-xs">

        <!-- tabulation -->
        <div class="tab-content">
            <div id="tous" class="tab-pane active" role="tabpanel">
                <?php
                $hotels = new WP_Query(
                    array("post_type"=>"hotels")
                );

                $hotels->the_post();
                $hotel1 = $hotels->post ;
                ?>
                <div class="row ">
                    <!--- offre 1 -->
                    <div class="col-md-12 offre">
                        <img src="<?php echo get_field('image_url')?>" class="background">
                        <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                        <button class="btn btn-default reserve"></button>
                        <div class="details-offre" >
                            <h3><?php  the_title() ;?></h3>
                            <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                            <div class="price">
                                <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                <p>A partir de </p>
                                <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                            </div>
                            <div class="enfant">
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" style="display:inline;float: left;width: 7%;">
                                <p style="display: inline;float: left"> -6 ans gratuit</p>
                            </div>
                        </div>
                    </div>
                    <!---- fin offre 1 -->

                </div>
                <div class="row">
                    <?php
                    $i = 1;
                    while ($hotels->have_posts()) : $hotels->the_post() ;
                        if ($i % 2 !=0) :
                            ?>

                            <div class="col-md-6 offre-sm" style="margin-right:2%">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                                <button class="btn reserve"></button>
                                <div class="details-offre" >
                                    <h3><?php  the_title() ;?></h3>
                                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                    <div class="price">
                                        <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                        <p>A partir de </p>
                                        <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                    </div>
                                    <div class="enfant">
                                        <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                        <p style="display: inline;float: left"> -6 ans gratuit</p>

                                    </div>
                                </div>
                            </div>
                        <?php  else : ?>
                            <div class="col-md-6 offre-sm" style="margin-left:2%">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                                <button class="btn reserve"></button>
                                <div class="details-offre" >
                                    <h3><?php  the_title() ;?></h3>
                                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                    <div class="price">
                                        <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                        <p>A partir de </p>
                                        <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                    </div>
                                    <div class="enfant">
                                        <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                        <p style="display: inline;float: left"> -6 ans gratuit</p>

                                    </div>
                                </div>
                            </div>

                            <?php
                        endif ;
                        $i++;
                    endwhile ;

                    ?>
                </div>

            </div>
            <!-- fin tab tous les offres pour les ecrans md et lg -->

            <!-- tab hammamet -->
            <div id="hammamet" class="tab-pane" role="tabpanel">
                <?php
                $hotels_hammamet = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Hammamet',
                            ),
                        ))
                );
                $i = 1;
                while ($hotels_hammamet->have_posts()) : $hotels_hammamet->the_post() ;
                    if ($i % 2 !=0) :
                        ?>

                        <div class="col-md-6 offre-sm" style="margin-right:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>
                    <?php  else : ?>
                        <div class="col-md-6 offre-sm" style="margin-left:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>

                        <?php
                    endif ;
                    $i++;
                endwhile ;

                ?>

            </div>
            <!-- fin tab hammamet -->

            <!-- tab sousse -->
            <div id="sousse" class="tab-pane" role="tabpanel">
                <?php
                $hotels_sousse = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Sousse',
                            ),
                        ))
                );
                $i = 1;
                while ($hotels_sousse->have_posts()) : $hotels_sousse->the_post() ;
                    if ($i % 2 !=0) :
                        ?>

                        <div class="col-md-6 offre-sm" style="margin-right:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>
                    <?php  else : ?>
                        <div class="col-md-6 offre-sm" style="margin-left:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>

                        <?php
                    endif ;
                    $i++;
                endwhile ;

                ?>

            </div>
            <!-- fin tab sousse -->

            <!-- tab monastir -->
            <div id="monastir" class="tab-pane" role="tabpanel">
                <?php
                $hotels_monastir = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Monastir',
                            ),
                        ))
                );
                $i = 1;
                while ($hotels_monastir->have_posts()) : $hotels_monastir->the_post() ;
                    if ($i % 2 !=0) :
                        ?>

                        <div class="col-md-6 offre-sm" style="margin-right:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>
                    <?php  else : ?>
                        <div class="col-md-6 offre-sm" style="margin-left:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>

                        <?php
                    endif ;
                    $i++;
                endwhile ;

                ?>
            </div>
            <!-- fin tab monastir -->

            <!-- tab mahdia -->
            <div id="mahdia" class="tab-pane" role="tabpanel">
                <?php
                $hotels_mahdia = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Mahdia',
                            ),
                        ))
                );
                $i = 1;
                while ($hotels_mahdia->have_posts()) : $hotels_mahdia->the_post() ;
                    if ($i % 2 !=0) :
                        ?>

                        <div class="col-md-6 offre-sm" style="margin-right:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>
                    <?php  else : ?>
                        <div class="col-md-6 offre-sm" style="margin-left:2%">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">
                            <button class="btn reserve"></button>
                            <div class="details-offre" >
                                <h3><?php  the_title() ;?></h3>
                                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                <div class="price">
                                    <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                    <p>A partir de </p>
                                    <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                </div>
                                <div class="enfant">
                                    <img style="display: inline; float: left; width: 10%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                    <p style="display: inline;float: left"> -6 ans gratuit</p>

                                </div>
                            </div>
                        </div>

                        <?php
                    endif ;
                    $i++;
                endwhile ;

                ?>
            </div>
            <!-- fin tab mahdia -->

        </div>
    </div>
    <div class="hidden-md hidden-lg hidden-sm visible-xs">
        <div class="tab-content">
            <div id="tous-sm" class="tab-pane active" role="tabpanel">
                <?php
                $hotels = new WP_Query(
                    array("post_type"=>"hotels")
                );
                while ($hotels->have_posts()) : $hotels->the_post() ;
                ?>
                <div class="row ">
                    <div class="col-sm-12 offre-sc">

                            <div class="row">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">

                                <div class="col-sm-12">
                                    <div class="details-offre">
                                        <h3><?php  the_title() ;?></h3>
                                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                        <div class="price">
                                            <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                            <p>A partir de </p>
                                            <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                        </div>
                                        <div class="enfant" style="text-align: center; display: block;">

                                            <img style="display: inline; float: left; margin-left: 40%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                            <p style="display: inline; float: left; color: #ffffff;">-6 ans gratuit</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center"><button class="btn btn-default btn-lg reserve">Réservez</button></div>
                            </div>

                    </div>
                </div>
                <?php endwhile ;?>
            </div>
            <!-- tab hammamet -->
            <div id="hammamet-sm" class="tab-pane" role="tabpanel">
            <?php
            $hotels_hammamet = new WP_Query(
                array("post_type"=>"hotels",
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key'     => 'ville',
                            'value'   => 'Hammamet',
                        ),
                    ))
            );
            while ($hotels_hammamet->have_posts()) : $hotels_hammamet->the_post() ;?>
                <div class="row ">
                    <div class="col-sm-12 offre-sc">

                        <div class="row">
                            <img src="<?php echo get_field('image_url')?>" class="background">
                            <img src="<?php echo get_field('ville_img')?>" class="ville-img">

                            <div class="col-sm-12">
                                <div class="details-offre">
                                    <h3><?php  the_title() ;?></h3>
                                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                    <div class="price">
                                        <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                        <p>A partir de </p>
                                        <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                    </div>
                                    <div class="enfant" style="text-align: center; display: block;">

                                        <img style="display: inline; float: left; margin-left: 40%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                        <p style="display: inline; float: left; color: #ffffff;">-6 ans gratuit</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center"><button class="btn btn-default btn-lg reserve">Réservez</button></div>
                        </div>

                    </div>
                </div>
            <?php endwhile ?>
            </div>
            <!-- fin tab hammamet -->

            <!-- tab sousse -->
            <div id="sousse-sm" class="tab-pane" role="tabpanel">
                <?php
                $hotels_sousse = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Sousse',
                            ),
                        ))
                );
                while ($hotels_sousse->have_posts()) : $hotels_sousse->the_post() ;?>
                    <div class="row ">
                        <div class="col-sm-12 offre-sc">

                            <div class="row">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">

                                <div class="col-sm-12">
                                    <div class="details-offre">
                                        <h3><?php  the_title() ;?></h3>
                                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                        <div class="price">
                                            <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                            <p>A partir de </p>
                                            <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                        </div>
                                        <div class="enfant" style="text-align: center; display: block;">

                                            <img style="display: inline; float: left; margin-left: 40%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                            <p style="display: inline; float: left; color: #ffffff;">-6 ans gratuit</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center"><button class="btn btn-default btn-lg reserve">Réservez</button></div>
                            </div>

                        </div>
                    </div>
                <?php endwhile ?>
            </div>
            <!-- fin tab sousse -->

            <!-- tab monastir -->
            <div id="monastir-sm" class="tab-pane" role="tabpanel">
                <?php
                $hotels_monastir = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Monastir',
                            ),
                        ))
                );
                while ($hotels_monastir->have_posts()) : $hotels_monastir->the_post() ;?>
                    <div class="row ">
                        <div class="col-sm-12 offre-sc">

                            <div class="row">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">

                                <div class="col-sm-12">
                                    <div class="details-offre">
                                        <h3><?php  the_title() ;?></h3>
                                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                        <div class="price">
                                            <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                            <p>A partir de </p>
                                            <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                        </div>
                                        <div class="enfant" style="text-align: center; display: block;">

                                            <img style="display: inline; float: left; margin-left: 40%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                            <p style="display: inline; float: left; color: #ffffff;">-6 ans gratuit</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center"><button class="btn btn-default btn-lg reserve">Réservez</button></div>
                            </div>

                        </div>
                    </div>
                <?php endwhile ?>
            </div>
            <!-- fin tab monastir -->

            <!-- tab mahdia -->
            <div id="mahdia-sm" class="tab-pane" role="tabpanel">
                <?php
                $hotels_mahdia = new WP_Query(
                    array("post_type"=>"hotels",
                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key'     => 'ville',
                                'value'   => 'Mahdia',
                            ),
                        ))
                );
                while ($hotels_mahdia->have_posts()) : $hotels_mahdia->the_post() ;?>
                    <div class="row ">
                        <div class="col-sm-12 offre-sc">

                            <div class="row">
                                <img src="<?php echo get_field('image_url')?>" class="background">
                                <img src="<?php echo get_field('ville_img')?>" class="ville-img">

                                <div class="col-sm-12">
                                    <div class="details-offre">
                                        <h3><?php  the_title() ;?></h3>
                                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" />
                                        <div class="price">
                                            <p class="old"><span class="barre"> <?php echo get_field('ancien_prix')?>  DT</span> LPD</p>
                                            <p>A partir de </p>
                                            <p class="new"><?php echo get_field('nouveau_prix')?> DT LPD</p>
                                        </div>
                                        <div class="enfant" style="text-align: center; display: block;">

                                            <img style="display: inline; float: left; margin-left: 40%;" src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/enfant.png" />
                                            <p style="display: inline; float: left; color: #ffffff;">-6 ans gratuit</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center"><button class="btn btn-default btn-lg reserve">Réservez</button></div>
                            </div>

                        </div>
                    </div>
                <?php endwhile ?>
            </div>
            <!-- fin tab mahdia -->

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="offres_theme">
            <h2 class="offres__theme_title">Nos offres par <span class="offre__char">t</span>hèmes</h2>
            <p class="offres__theme__description">Spécial famille, séjour de noce ou encore petits Budget... Réservez facilement le séjour qui vous correspond grâce à nos sélections d'hôtels par thème</p>


            <hr class="offres__theme_hr" />

        </div>
        <div class="col-md-12">
            <div class="col-md-7">
                <div class="img__room"><img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/room.png" /></div>
            </div>
            <div class="col-md-5">
                <?php
                $offres = new WP_Query(
                    array("post_type"=>"offretheme")
                );
                while ($offres->have_posts()) : $offres->the_post() ;
                ?>

                <div class="offres_example">

                        <?php
                        if(get_field('promotion')==1) : ?>
                           <a href="<?php the_permalink(); ?>"> <h3 class="offres_example__promo"><?php echo get_field('nom') ?> </h3></a>
                        <?php
                            else : ?>
                            <a href="<?php the_permalink(); ?>"><h3><?php echo get_field('nom') ?> </h3> <a/>
                        <?php
                        endif;?>

                    <p class="offres__example__description"><?php echo get_field('description')?></p>
                </div>

                <?php  endwhile?>
                <div id="myCarousel" class="offres_example_nav"></div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>


<?php

/* Template name: custom search */
 get_header() ;
$personnes = 0;
$enfants=0;
$adultes=0;
 if($_GET['destination'] && !empty($_GET['destination'])) {
     $destination = $_GET['destination'];
 }
if($_GET['adultes'] && !empty($_GET['adultes'])) {
    $adultes = $_GET['adultes'];
    $personnes+=$adultes;
}
if($_GET['enfants'] && !empty($_GET['enfants'])) {
    $enfants = $_GET['enfants'];
    $personnes+=$enfants;
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/background-couples.png" width="100%" height="220px">
        </div>
        <div class="col-md-12 best-of">
            <div >
                <h1 style="text-align: center;">Liste des <span style="color: red;">H</span>ôtels à <?php echo ucfirst($destination) ?></h1>
                <p style="text-align: center; margin-bottom: 5px;">Acceuil <span style="color: red;"> > </span>
                    <span class="current">Hôtels à <?php echo ucfirst($destination) ?> </span> </p>
                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/line.png" alt="" width="170" height="2" />

            </div>
        </div>
        <div class="col-md-12">
            <div class="search-details text-center" >
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        <h3>Votre <br> Recherche</h3>
                    </div>
                    <div class="col-md-2">
                        <p>1 Hôtel(s) trouvé(s) <br>
                            <?php echo ucfirst($destination) ?>
                        </p>

                    </div>
                    <div class="col-md-1">
                        <p>A partir de <br> <span style="color:#e9ac4c">16/05/2017</span></p>
                    </div>
                    <div class="col-md-1">
                        <p>Jusqu'au <br> <span style="color:#e9ac4c">17/05/2017</span></p>
                    </div>
                    <div class="col-md-1">
                        <p><span style="color:#e9ac4c">1 x(<?=$adultes ?> Adulte(s)<?php if($enfants>0) echo " + ".$enfants." Enfant(s)" ;?>)</span></p>
                    </div>
                    <div class="col-md-1">
                        <button class="btn search-btn">Modifier</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="advanced-search">
                <h4>Recherche Avancée</h4>
                <h6>Trier par </h6>
                <div class="hidden-xs">
                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/checked.png"><span class="search-method"> Prix </span>
                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/inchecked.png"> <span class="search-method"> Nom d'hotel</span>
                    <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/checked.png"> Catégorie
                </div>

                <div class="row hidden-lg hidden-md hidden-sm">
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/checked.png"><span class="search-method"> Prix </span>
                    </div>
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/inchecked.png" > <span class="search-method"> Nom d'hotel</span>
                    </div>
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/07/checked.png"> Catégorie
                    </div>
                </div>
                <hr class="divider">
                <h6>Filtrer par </h6>
                <div class="row">
                    <div class="col-md-2">
                        <select class="form-control">
                            <option>Tous les hôtels</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control">
                            <option>Toutes les localités</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control">
                            <option>Toutes les catégories</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control">
                            <option>Tous les arrangements</option>
                        </select>
                    </div>
                </div>
                <hr class="divider">
                <h6>Budget</h6>
                <div class="row">
                    <div class="col-md-1 col-sm-3" >
                        <span class="price"> 38.703 DT</span>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="sr-only">20% Complete (danger)</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <span class="price"> 441.703 DT</span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row hotels-container" >
        <?php
        $hotels = new WP_Query(
            array("post_type" => "hotels",
                'meta_query' => array(
                    'relation' => 'OR',
                    array(
                        'key' => 'ville',
                        'value' =>$destination,
                        'compare'=>'LIKE'
                    ),array(
                        'key' => 'nom',
                        'value' =>$destination,
                        'compare'=>'LIKE'
                    ),
                ))
        );
        while ($hotels->have_posts()) : $hotels->the_post() ;

        ?>
        <div class="col-md-12 search-item-list" >
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="hotel_img">
                        <img src="<?php echo get_field('image_url')?>" class="hotel__details__image">

                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="hotel__details">
                        <h3><?php  the_title() ;?></h3>
                        <h4><?php echo get_field('ville')?> Tunisie</h4>
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" class="vote">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/like.png" class="like">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/like_.png" class="like">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/medal.png" class="like">
                        <p>
                            <?php echo get_field('description')?>
                        </p>
                    </div>

                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <div class="details-price text-center">
                        <p>A partir de </p>
                        <p class="price"><?php echo get_field('nouveau_prix')?>  DT </span></p>
                        <button class="btn search-btn">Reserver </button>
                    </div>
                </div>
            </div>

        </div>

    <?php   endwhile; ?>


    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-5">
            <nav aria-label="sarch result page" id="search-dist-result">
                <ul class="pagination">

                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>

                </ul>
            </nav>
        </div>
    </div>

</div>


<?php
get_footer();
?>

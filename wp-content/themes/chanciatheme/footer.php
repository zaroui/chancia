<div class="container-fluid">
    <div class="row">
                <div class="offres_theme">
                    <h2 class="offres__theme_title">Ils ont  <span class="offre__char">p</span>arlé de nous</h2>
                    <p class="offres__theme__description">Médis et articles de presse qui ont parlé de nous dans le cadre du lancement de notre marque, de la médiatisation de nos axes et nos nouveautés </p>
                    <hr class="offres__theme_hr">
                </div>

                <div class="menu__defilie">
                    <div class="col-xs-12">
                        <div class="col-xs-1">
                            <img src="<?php bloginfo('template_url'); ?>/img/left.png" class="img-responsive img-center">
                        </div>

                        <div class="col-xs-2">
                            <img src="<?php bloginfo('template_url'); ?>/img/01.png" class="img-responsive img-center">
                        </div>
                        <div class="col-xs-2">
                            <img src="<?php bloginfo('template_url'); ?>/img/02.png" class="img-responsive img-center">
                        </div>
                        <div class="col-xs-2">
                            <img src="<?php bloginfo('template_url'); ?>/img/03.png" class="img-responsive img-center">
                        </div>
                        <div class="col-xs-2">
                            <img src="<?php bloginfo('template_url'); ?>/img/04.png" class="img-responsive img-center">
                        </div>
                        <div class="col-xs-2">
                            <img src="<?php bloginfo('template_url'); ?>/img/05.png" class="img-responsive img-center">
                        </div>

                        <div class="col-xs-1" ><img src="<?php bloginfo('template_url'); ?>/img/right.png" class="img-responsive img-center"></div>
                    </div>

                </div>
            </div>
            <div  class="row">
                <div class="offre__avis">
                    <img src="<?php bloginfo('template_url'); ?>/img/offre_avis.png">
                    <h2 class="offre__avis__title">Pas encore convaincu ? <br>Pourquoi réserver chez Chancia.com</h2>
                    <button class="btn btn__offre_avis">j'y vais</button>
                </div>
            </div>

            <div  class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="offres_theme offres__footer">
                            <h2 class="offres__theme_title" style="text-align: left;">Top des <span class="offre__char">20</span> hôtels</h2>
                            <div class="col-xs-6">
                                <p class="offres__theme__description_footer">
                                    Le Royal Hammamet<br>
                                    Africa Jade Thalasso<br>
                                    Laico Hammamet<br>
                                    Oceana Hammamet<br>
                                    Radisson Blu Resort<br> 
                                    & Thalasso Hammamet<br>
                                    THE RUSSELIOR<br>
                                    SENTIDO Le Sultan<br>
                                    El Mouradi Hammamet<br>
                                    Palace Hammamet Marhaba<br>
                                    La Playa Hôtel Club<br>
                                    Paradis Palace<br>
                                    Sousse Palace<br>
                                    Alassio Kantaoui<br>
                                </p>
                            </div>
                            <div class="col-xs-6">
                                <p class="offres__theme__description_footer" style="padding-left: 0px;">
                                    Alassio Kantaoui<br>
                                    El kantaoui Center<br>
                                    El Mouradi Club Kantaoui<br>
                                    Hannibal Palace<br>
                                    Houria Palace<br>
                                    Marhaba Royal Salem<br>
                                    El Mouradi Palace<br>
                                    Royal Thalassa Monastir<br>
                                    Skanes Serail<br>
                                    Thapsus Club<br>
                                    Isis Thalasso & Spa<br>
                                    Djerba Plaza<br>
                                    Green Palm Golf & Spa<br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="offres_theme offres__footer" style="padding-left: 0px !important;">
                            <h2 class="offres__theme_title contact_style" style="text-align: left;">Contactez-<span class="offre__char">nous</span></h2>
                            <div class="icon__contact_footer"><img src="<?php bloginfo('template_url'); ?>/img/immeuble.png"> <p class="description_footer">Immeuble SOFIDE, 3ème étage<br>Angle rue des Entrepreneurs <br>et rue du Métal <br>Z.I Charguia II – 2035  <br>TUNIS CARTHAGE</p>
                            </div>
                            <br><br>
                            <div class="icon__contact_footer"><img src="<?php bloginfo('template_url'); ?>/img/phone.png"> <p class="description_footer">70 103 100<br><br></p>
                            </div>
                            <div class="col-xs-12"><div class="col-xs-12"><img class ="coc__img" src="<?php bloginfo('template_url'); ?>/img/coccinelle.png"></div></div>
                            <div class="col-xs-12"><img class="store__icon" src="<?php bloginfo('template_url'); ?>/img/google_play.png"><img class="store__icon apple_icon" src="<?php bloginfo('template_url'); ?>/img/app_store.png"></div>
                        </div>
                    </div>
                </div>

            </div><!-- /.container -->
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="footer">
                    <div class="col-xs-7">
                        <p class="text_footer">© 2017 Copyright chancia.com Tous droit réservés l Chancia est une marque exclusive de Splendid Tour SA</p>
                    </div>
                    <div class="col-xs-5">
                        <p><img class="rsocial__icon" src="<?php bloginfo('template_url'); ?>/img/facebook.png"> <img class="rsocial__icon" src="<?php bloginfo('template_url'); ?>/img/twitter.png"> <img class="rsocial__icon" src="<?php bloginfo('template_url'); ?>/img/pwebsite.png"> <img class="rsocial__icon" src="<?php bloginfo('template_url'); ?>/img/t-twit.png"> <img class="rsocial__icon" src="<?php bloginfo('template_url'); ?>/img/instagram.png"></p>
                    </div>

                </div>
            </div>
        </div>
</div> <!-- end wrapper content -->
</div><!-- end wrapper -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="<?php bloginfo('template_url'); ?>/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap-datetimepicker.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap-datetimepicker.fr.js"></script>
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                    language:  'fr',
                    format: 'DD, dd MM',
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
                });
            });

        </script>

</body>
</html>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <title>Chancia</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php bloginfo('template_url'); ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css" type="text/css" media="screen" />


        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">

                <li>
                    <a href="<?php echo get_page_link(4) ?>"> About Us </a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row top-bar hidden-sm hidden-xs">
                    <div class="col-md-4 col-sm6 col-xs-12">
                        <div class="mail">
                            <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>  contact@chancia.com</a>
                        </div>

                    </div>
                    <div class="col-md-4 hidden-sm hidden-xs "></div>
                    <div class="col-md-4 col-sm6 col-xs-12 social-networ" >
                        <ul >
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a> </li>
                            <li><a href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a> </li>
                            <li><a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button href="#menu-toggle"  class="btn navbar-toggle" id="menu-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo get_page_link(7) ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" class="logo"> </a>
                        <li class="phone-number hidden-xs"><img src="<?php bloginfo('template_url'); ?>/img/phone.png " class="hidden-xs"> 70 103 100 </li>
                    </div>


                </div><!-- /.container-fluid -->
            </nav>
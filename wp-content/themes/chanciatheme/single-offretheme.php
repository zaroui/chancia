<?php

get_header()  ;
$current = get_the_ID() ;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/background-couples.png" width="100%" height="220px">
        </div>
        <div class="col-md-12 best-of">
            <div >
                <h1 style="text-align: center;">Offre <span style="color: red;">C</span>ouples</h1>
                <p style="text-align: center; margin-bottom: 5px;">Acceuil <span style="color: red;"> > </span>
Idées séjours <span style="color: red;"> > </span>
                    <span class="current"><?php echo get_field('nom') ?> </span>  </p>
                <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/line.png" alt="" width="170" height="2" />

            </div>
        </div>

        <div class="col-md-12">
            <div class="search-details text-center" >
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        <h3>Votre <br> Recherche</h3>
                    </div>
                    <div class="col-md-2">
                        <p>1 Hôtel(s) trouvé(s) <br>
                            Hammamet, Tunisie
</p>

                    </div>
                    <div class="col-md-1">
                        <p>A partir de <br> <span style="color:#e9ac4c">16/05/2017</span></p>
                    </div>
                    <div class="col-md-1">
                        <p>Jusqu'au <br> <span style="color:#e9ac4c">17/05/2017</span></p>
                    </div>
                    <div class="col-md-1">
                        <p><span style="color:#e9ac4c">1 x(2 Adulte(s))</span></p>
                    </div>
                    <div class="col-md-1">
                        <button class="btn search-btn">Modifier</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row hotels-container" >
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 style="color : #ff0a00;">Notre sélection d'Hôtel </h3>
            <p>pour couples</p>
            <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/destination-line.png">
        </div>
        <?php
        $hotels = new WP_Query(
            array("post_type"=>"hotels" )
        );
        while ($hotels->have_posts()) : $hotels->the_post() ;
            $theme = get_field('theme');
            if($theme[0]->ID==$current) :
        ?>

        <div class="col-md-12 col-sm-12 col-xs-12 search-item-list" >
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="hotel_img">
                        <img src="<?php echo get_field('image_url')?>" class="hotel__details__image">
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="hotel__details">
                        <h3><?php echo get_field('nom')?></h3>
                        <h4><?php echo get_field('ville')?> Tunisie</h4>
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/vote.png" class="vote">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/like.png" class="like">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/like_.png" class="like">
                        <img src="http://127.0.0.1/chancia/wp-content/uploads/2017/06/medal.png" class="like">
                        <p>
                            <?php echo get_field('description')?> <a href="#" >>> plus de détails </a>
                        </p>
                    </div>

                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <div class="details-price text-center">
                        <p>A partir de </p>
                        <p class="price">197<span class="float">.977  DT </span></p>
                        <button class="btn search-btn">Reserver </button>
                    </div>
                </div>
            </div>

        </div>
        <?php
        endif;
        endwhile; ?>

    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-5">
            <nav aria-label="sarch result page" id="search-dist-result">
                <ul class="pagination">

                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>

                </ul>
            </nav>
        </div>
    </div>

</div>
<?php get_footer() ; ?>
